import os
import simplejson as json
from datetime import datetime
import ipdb

import tweepy
import pymongo

# Go to http://dev.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key = '123'
consumer_secret = '456'

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token = 'abc'
access_token_secret = 'def'

base_dir = 'data/test'

class MyModelParser(tweepy.parsers.ModelParser):
    """ To get raw json from tweepy output
    ref: https://gist.github.com/inactivist/5263501 
    """
    def parse(self, method, payload):
        result = super(MyModelParser, self).parse(method, payload)
        result._payload = json.loads(payload)
        return result

if __name__ == '__main__':
    # setup oauth
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    # initialize mongodb
    client = pymongo.MongoClient()
    db = client.test_db
    twt_collection = db.tweets

    api = tweepy.API(auth, parser=MyModelParser())
    query = 'statue of liberty'
    page_count = 0

    for result in tweepy.Cursor(api.search, q=query, count=100, include_entities=True, lang='en').pages():
        raw_json = result._payload
        tweets = raw_json['statuses']
        for tweet in tweets:
            # write to mongo
            print tweet['text']
            obj_id = twt_collection.insert(tweet)

        # stop after a certain number of pages
        page_count += 1
        if page_count >= 10:
            break
