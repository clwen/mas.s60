pegs = [1, 2, 3]

def hanoi_tower(disks, source, target):
    """ Conduct Hanoi Tower movement and print the procedure.
    Assuming that number of pegs is three.
    Args:
        disks: a list of disks in ascending order of size
        source: source peg
        target: target peg
    """
    if len(disks) == 0: 
        return
    elif len(disks) == 1:
        print "move disk %s from %s to %s" % (disks[0], source, target)
    else: # recursive case
        # get the empty peg as buffer
        buffer_peg = [p for p in pegs if p != source and p != target][0]
        # move all the rest to the empty peg
        hanoi_tower(disks[:-1], source, buffer_peg)
        # move the biggest to the target peg
        hanoi_tower(disks[-1:], source, target) # NOTE: a trail colon to make it a list
        # move all the rest back to target peg
        hanoi_tower(disks[:-1], buffer_peg, target)

if __name__ == '__main__':
    disks = [1, 2, 3, 4, 5]
    hanoi_tower(disks, 1, 2)
