
def char_to_int(c):
    """ Return the integer represents the char input in 10-base """
    if c.isdigit():
        return (ord(c) - ord('0'))
    else: # A ~ F
        return (ord(c) - ord('A') + 10)

def int_to_char(n):
    if n < 10:
        return str(n)
    else: # A ~ F
        return chr(ord('A') + (n-10))

def to_10_base(s, base):
    result = 0
    for i, c in enumerate(s):
        result *= base
        lsd = char_to_int(c)
        result += lsd
    return result

def from_10_base(n, base):
    result = ''
    while n > 0:
        lsd = int_to_char(n % base)
        result += lsd
        n /= base
    return result[::-1]

def base_conversion(s, b1, b2):
    """ Base conversion from base b1 to base b2
    Args:
        s: the number in string format
        b1: base 1 (2 ~ 16)
        b2: base 2 (2 ~ 16)
    """
    # from b1 to 10-based
    s_10 = to_10_base(s, b1)
    # from 10-based to b2
    s_b2 = from_10_base(s_10, b2)

    return s_b2
