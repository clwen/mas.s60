
def from_ten_base(s, base):
    result = ''
    while s:
        r = s % base
        if r < 10:
            result += str(r)
        else: 
            result += chr(ord('A') + (r - 10))
        s /= base
    return result[::-1]

def to_ten_base(s, base):
    result = 0
    power = 0
    for c in s[::-1]:
        if int(c) < 10:
            cur_digit = int(c)
        else:
            cur_digit = chr(ord('A') + (int(c) - 10))
        result += cur_digit * (base ** power)
        power += 1
    return result

def base_conversion(b1, s, b2):
    """ Convert a string s from base b1 to base b2 
    Args:
        b1: base 1
        s: string to convert
        b2: base 2
    """
    # convert s in b1 to 10-base first
    s_ten_base = to_ten_base(s, b1)
    print "s in ten base: %s" % (s_ten_base)
    # convert 10-based s to b2
    s_in_b2 = from_ten_base(s_ten_base, b2)
    return s_in_b2

if __name__ == '__main__':
    print base_conversion(8, '765', 16)
