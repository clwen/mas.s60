import ipdb

def power_set(lst):
    # base cases: empty list or 1-element list
    if not lst:
        return [[]]
    elif len(lst) == 1:
        return [[], [lst[0]]]

    # recursive cases:
    first = lst[0]
    rest = lst[1:]
    power_set_rest = power_set(rest)
    result = []
    for subset in power_set_rest:
        with_first = subset[:]
        with_first.append(first)
        result.append(with_first)

        without_first = subset[:]
        result.append(without_first)
    return result

if __name__ == '__main__':
    result = power_set([1, 2, 3, 4, 5])
    print result
    print "number of subset: %s" % (len(result))
