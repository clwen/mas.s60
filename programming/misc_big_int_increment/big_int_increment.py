import ipdb

def big_int_increment(bi):
    carry = 1
    for i in reversed(xrange(len(bi))): # TODO: index incorrect, find a better way to traverse backward
        rd = bi[i] + carry
        bi[i] = rd % 10
        carry = 1 if rd >= 10 else 0
        if carry == 0:
            break

    # if there is a carry remains, put a one in front of the list
    if carry == 1:
        bi = [1] + bi

    return bi

if __name__ == '__main__':
    print big_int_increment([1, 2, 3])
    print big_int_increment([9, 9, 9])
