A compilation of mas.s60 materials

### Programming
Picked a small sample of my solution to some programming puzzles. Please see the `programming` folder.

### Software Dev
This repo

### Web Dev
Please see the `web` folder for a blog written in Flask

### Data
Please see the `data` folder for a twitter crawler that store tweets to MongoDB

### Mobile
I don't want to bind myself to a particular framework. I wrote a hello world app in Object-C. Please see the `ios` folder

### Visualization
I am quite familiar with D3. Attached a video of a visualization project I've done in a hackathon. Didn't attach the code for confidentiality issue. Please see the `visualization` folder.
