//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Chunglin Wen on 12/2/13.
//  Copyright (c) 2013 clwen apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
